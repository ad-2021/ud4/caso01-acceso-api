package accesoapi;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class metodoDELETE {
	public static void main(String[] args) {

		try {
			borraCliente(41);
			System.out.println("¡Borrado!");
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}

	}

	private static void borraCliente(int id) {
		URL url;
		HttpURLConnection con;

		try {
			url = new URL("http://localhost:8080/clientes/" + id);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("DELETE");
			//con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// System.out.println("Fallo leyendo recurso. Código de error: " +
				// con.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			}
			con.disconnect();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
