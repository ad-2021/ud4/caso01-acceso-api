package accesoapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

// https://mkyong.com/webservices/jax-rs/restfull-java-client-with-java-net-url/
public class metodoGET {
	public static void main(String[] args) {

		try {
			Cliente cliente = dameCliente(1);
			System.out.println(cliente);
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("--------------------------");
		
		try {
			List<Cliente> clientes = dameClientes();	
			clientes.stream().forEach(System.out::println);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static Cliente dameCliente(int id) {
		String lin, salida = "";
		Cliente cliente = null;
		Gson gson;
		URL url;
		HttpURLConnection con;

		try {
			url = new URL("http://localhost:8080/clientes/" + id);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// System.out.println("Fallo leyendo recurso. Código de error: " +
				// con.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
				while ((lin = br.readLine()) != null) {
					salida = salida.concat(lin);
				}
				gson = new Gson();
				cliente = gson.fromJson(salida, Cliente.class);				
			}
			con.disconnect();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return cliente;
	}

	private static List<Cliente> dameClientes() {
		String lin, salida = "";
		Cliente[] clientes;
		List<Cliente> listClientes = null;
		Gson gson;
		URL url;
		HttpURLConnection con;

		try {
			url = new URL("http://localhost:8080/clientes");
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
				// System.out.println("Fallo leyendo recurso. Código de error: " +
				// con.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
				while ((lin = br.readLine()) != null) {
					salida = salida.concat(lin);
				}

				gson = new Gson();
				clientes = gson.fromJson(salida, Cliente[].class);
				listClientes = new ArrayList<Cliente>(Arrays.asList(clientes));
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}				
			}
			con.disconnect();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return listClientes;
	}
}
