package accesoapi;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

/**
 * Hello world!
 *
 */
public class metodoPOST {
	public static void main(String[] args) {
		Gson gson;
		try {
			gson = new Gson();
			Cliente cliente = new Cliente("nnnn", "direccion nnnn, 12");
			String clienteJson = gson.toJson(cliente, Cliente.class);
			enviaCliente(clienteJson);
			System.out.println("¡Creado!");
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
		

	}

	private static void enviaCliente(String clienteJson) {	
		URL url;
		HttpURLConnection con;

		try {
			url = new URL("http://localhost:8080/clientes");
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			OutputStream os = con.getOutputStream();
			os.write(clienteJson.getBytes());
			os.flush();
			
			if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			} 				
			con.disconnect();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	
}
